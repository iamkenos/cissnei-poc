@DEMO
Feature: Drag and Drop

  Scenario: Drag element A to element B
    Given the page "https://the-internet.herokuapp.com/drag_and_drop" is opened
    When the user drags the box "A" to box "B"
    Then the boxes "A" and "B" are swapped