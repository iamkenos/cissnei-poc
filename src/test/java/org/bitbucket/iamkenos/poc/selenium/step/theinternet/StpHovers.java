package org.bitbucket.iamkenos.poc.selenium.step.theinternet;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.bitbucket.iamkenos.poc.selenium.page.theinternet.ObjHovers;

import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpHovers {
    private ObjHovers objHovers;

    public StpHovers() {
        objHovers = new ObjHovers();
    }

    @When("^the user hovers on the \"([^\"]*)\" image$")
    public void the_user_hovers_on_the_image(String arg1) throws Exception {
        driver.focus(objHovers.getUserImage(arg1));
    }

    @Then("^the the \"([^\"]*)\" hover details is displayed$")
    public void the_the_hover_details_is_displayed(String arg1) throws Exception {
        driver.thenElemVisible(objHovers.getUserHoverDetails(arg1));
    }

}
