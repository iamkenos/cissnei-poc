package org.bitbucket.iamkenos.poc.rest;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.bitbucket.iamkenos.cissnei.runner.ParentRunner;
import org.junit.runner.RunWith;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        //dryRun = true,
        features = {"features/rest/"},
        glue = {"classpath:org/bitbucket/iamkenos/poc/rest/"},
        tags = {"@DEMO"}
)
public final class RestTest extends ParentRunner {
}
