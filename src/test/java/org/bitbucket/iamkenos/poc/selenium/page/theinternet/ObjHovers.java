package org.bitbucket.iamkenos.poc.selenium.page.theinternet;

import org.openqa.selenium.By;

import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.TO_REPLACE;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ObjHovers {
    private String xpathUserFigure = String.format("//*[@class='figure']//*[contains(.,'%s')]", TO_REPLACE);
    private String xpathUserImg = String.format("%s//../img", xpathUserFigure);
    private String xpathUserDetails = String.format("%s[@class='figcaption']", xpathUserFigure);

    public By getUserImage(String user) {
        return By.xpath(xpathUserImg.replace(TO_REPLACE, user));
    }

    public By getUserHoverDetails(String user) {
        return By.xpath(xpathUserDetails.replace(TO_REPLACE, user));
    }
}

