package org.bitbucket.iamkenos.poc.selenium.page.seleniumframework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;


public final class ObjPracticeForm {

    private By btnsGreen = By.xpath("//button[contains(@style,'DarkGreen')]|//button[contains(@style,'darkgreen')]");
    private By parentWrapper = By.xpath("//h2//..");
    private By nestedElement = By.xpath("//p[contains(.,'This is a nested element')]");
    private By randomId = By.xpath("//p[contains(text(),'random')]");

    private List<WebElement> greenBtns() {
        return driver.findElements(btnsGreen);
    }

    public void clickOnGreenBtn(String text) {
        WebElement e = driver.findElementFromListWithText(greenBtns(), text);
        driver.click(e);
    }

    public void clickOnLink(String text) {
        driver.click(By.linkText(text));
    }

    public void closeBrowserWindow(String text) {
        driver.switchToWindowWithText(text);
        driver.embedText(driver.getCurrentUrl());
        driver.close();
        driver.switchToParentWindow();
    }

    public void closeMessageWindow(String text) {
        driver.switchToWindowWithText(text);
        driver.embedText(driver.getPageSource());
        driver.close();
        driver.switchToParentWindow();
    }

    public void closeBrowserTab(String text) {
        driver.switchToWindowWithText(text);
        driver.embedText(driver.getCurrentUrl());
        driver.close();
        driver.switchToParentWindow();
    }

    public void printNestedText() {
        WebElement e = driver.findNestedElement(parentWrapper, nestedElement);
        driver.embedText(driver.getElementText(e));
    }

    public void printRandomId() {
        WebElement e = driver.findElement(randomId);
        driver.embedText(driver.getElementText(e) + ": " + driver.getAttribute(e, "id"));
    }

    public void printAlertMessage() {
        driver.embedText(driver.getAlert().getText());
    }

    public void closeAlert() {
        driver.alertDismiss();
    }

    public void dragButtons(String dragFrom, String dragTo) {
        WebElement e = driver.findElementFromListWithText(greenBtns(), dragFrom);
        WebElement f = driver.findElementFromListWithText(greenBtns(), dragTo);
        driver.dragAndDrop(e, f);
    }
}
