package org.bitbucket.iamkenos.poc.selenium.page.theinternet;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.TO_REPLACE;
import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ObjSortableDataTables {
    private String headerSortDown = "headerSortDown";
    private String headerSortUp = "headerSortUp";
    private String xpathTableHeaders = String.format("//table[@id='table1']//th[contains(@class,'header')]/span", TO_REPLACE);
    private String xpathTableColumn = String.format("%s[text()='%s']", xpathTableHeaders, TO_REPLACE);
    private String xpathTableColumnParent = xpathTableColumn + "/..";
    private String xpathTableColumnValues = String.format("//table[@id='table1']//td[%s]", TO_REPLACE);
    public List<String> amountCols = newArrayList("Due");


    public void sortAscending(String label) {
        while (!isAscending(label)) {
            driver.click(getTableColumn(label));
        }
    }

    public void sortDescending(String label) {
        while (!isDescending(label)) {
            driver.click(getTableColumn(label));
        }
    }

    public By getTableColumn(String label) {
        return By.xpath(xpathTableColumn.replace(TO_REPLACE, label));
    }

    public By getTableColumnParent(String label) {
        return By.xpath(xpathTableColumnParent.replace(TO_REPLACE, label));
    }

    public Boolean isAscending(String label) {
        return driver.isElemAttribContaining(getTableColumnParent(label), "class", headerSortDown);
    }

    public Boolean isDescending(String label) {
        return driver.isElemAttribContaining(getTableColumnParent(label), "class", headerSortUp);
    }

    private Integer getColumnHeaderIndex(String label) {
        List<String> headers = driver.findElements(By.xpath(xpathTableHeaders))
                .stream()
                .map(element -> driver.getElementText(element))
                .collect(Collectors.toList());

        return headers.indexOf(label) + 1;
    }

    private By getColumnValuesLocator(String label) {
        return By.xpath(xpathTableColumnValues.replace(TO_REPLACE, getColumnHeaderIndex(label).toString()));
    }

    public <T> List<T> getColumnValues(String label) {
        if (amountCols.contains(label)) {
            return driver.findElements(getColumnValuesLocator(label))
                    .stream()
                    .map(element -> (T) Double.valueOf(driver.getElementText(element).replace("$", StringUtils.EMPTY)))
                    .collect(Collectors.toList());
        } else
            return driver.findElements(getColumnValuesLocator(label))
                    .stream()
                    .map(element -> (T) driver.getElementText(element))
                    .collect(Collectors.toList());
    }
}