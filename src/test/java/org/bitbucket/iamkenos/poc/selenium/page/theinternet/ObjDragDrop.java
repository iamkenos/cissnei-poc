package org.bitbucket.iamkenos.poc.selenium.page.theinternet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ObjDragDrop {
    public By divColumnA = By.id("column-a");
    public By divColumnB = By.id("column-b");
    private By divColumns = By.xpath("//*[@class='column']");

    public void dragDrop(String source, String target) {
        driver.dragAndDrop(getBoxWithText(source), getBoxWithText(target));
    }

    public WebElement getBoxWithText(String text) {
        return driver.findElementFromListWithText(divColumns, text);
    }
}
