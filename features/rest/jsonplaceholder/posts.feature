@DEMO
Feature: Posts API

  Scenario: Create a new post on the Json Placeholder site using a bean
    Given the user creates a new post:
      | userId | 10                          |
      | id     | 101                         |
      | title  | lorem ipsum                 |
      | body   | consectetur adipiscing elit |
    Then the post is created correctly

  Scenario: Delete a newly created post on the Json Placeholder site and let it fail
    Given the user creates a new post:
      | userId | 10                          |
      | id     | 101                         |
      | title  | lorem ipsum                 |
      | body   | consectetur adipiscing elit |
    Then the post is created correctly
    When the user deletes the created post
    Then the post is created

  Scenario: Delete a newly created post on the Json Placeholder site
    Given the user creates a new post:
      | userId | 10                          |
      | id     | 101                         |
      | title  | lorem ipsum                 |
      | body   | consectetur adipiscing elit |
    Then the post is created correctly
    When the user deletes the created post
    Then the post is deleted

  Scenario: Update a newly created post on the Json Placeholder site
    Given the user creates a new post:
      | userId | 10                          |
      | id     | 101                         |
      | title  | lorem ipsum                 |
      | body   | consectetur adipiscing elit |
    Then the post is created correctly
    When the user updates the created post:
      | userId | 10                          |
      | id     | 101                         |
      | title  | lorem ipsum                 |
      | body   | consectetur adipiscing elit |
    Then the post is updated