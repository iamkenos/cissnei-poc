package org.bitbucket.iamkenos.poc.rest;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class RestHook {
    public static Scenario scenario;

    @Before
    public void beforeScenario(Scenario myScenario) {
        scenario = myScenario;
    }

    @After
    public void afterScenario() {
    }
}
