package org.bitbucket.iamkenos.poc.rest.api.jsonplaceholder;

import io.restassured.http.ContentType;
import org.bitbucket.iamkenos.cissnei.rest.Client;
import org.bitbucket.iamkenos.poc.rest.model.jsonplaceholder.Post;

import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.transformData;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ApiPosts {
    private Client client;
    private Post post;

    private String epPosts = "posts";

    public ApiPosts(Client client) {
        this.client = client;
        this.client.getInstanceConfig()
                .setBaseUri(transformData("{API_DEMO URL}"));
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public void createPost(String body) {
        client.body(body)
                .contentType(ContentType.JSON)
                .post(epPosts);
    }

    public void deletePost(String id) {
        client.delete(epPosts + "/" + id);
    }

    public void updatePost(String id, String body) {
        client.contentType(ContentType.JSON)
                .body(body)
                .put(epPosts + "/" + id);
    }
}
