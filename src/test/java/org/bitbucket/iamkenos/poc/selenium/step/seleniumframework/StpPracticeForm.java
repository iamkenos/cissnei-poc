package org.bitbucket.iamkenos.poc.selenium.step.seleniumframework;

import cucumber.api.java.en.When;
import org.bitbucket.iamkenos.poc.selenium.page.seleniumframework.ObjPracticeForm;

public final class StpPracticeForm {
    private ObjPracticeForm objPracticeForm;

    public StpPracticeForm() {
        objPracticeForm = new ObjPracticeForm();
    }

    @When("^user clicks on \"([^\"]*)\" button$")
    public void user_clicks_on_button(String arg1) throws Exception {
        objPracticeForm.clickOnGreenBtn(arg1);
    }

    @When("^user clicks on the \"([^\"]*)\" link$")
    public void user_clicks_on_the_link(String arg1) throws Exception {
        objPracticeForm.clickOnLink(arg1);
    }

    @When("^user closes the new browser window$")
    public void user_closes_the_new_browser_window() throws Exception {
        objPracticeForm.closeBrowserWindow("Agile Testing and ATDD Automation");
    }

    @When("^user closes the new message window$")
    public void user_closes_the_new_message_window() throws Exception {
        objPracticeForm.closeMessageWindow("This message window is only for viewing purposes");
    }

    @When("^user closes the new browser tab$")
    public void user_closes_the_new_browser_tab() throws Exception {
        objPracticeForm.closeBrowserTab("Agile Testing and ATDD Automation");
    }

    @When("^user prints the nested element text$")
    public void user_prints_the_nested_element_text() throws Exception {
        objPracticeForm.printNestedText();
    }

    @When("^user prints the random id$")
    public void user_prints_the_random_id() throws Exception {
        objPracticeForm.printRandomId();
    }

    @When("^user prints the alert message$")
    public void user_prints_the_alert_message() throws Exception {
        objPracticeForm.printAlertMessage();
    }

    @When("^user closes the alert$")
    public void user_closes_the_alert() throws Exception {
        objPracticeForm.closeAlert();
    }

    @When("^user drags the \"([^\"]*)\" button to \"([^\"]*)\" button$")
    public void user_drags_the_button_to_button(String arg1, String arg2) throws Exception {
        objPracticeForm.dragButtons(arg1, arg2);
    }
}
