@DEMO
Feature: File Upload

  Scenario Outline: Upload the <File> file
    Given the page "https://the-internet.herokuapp.com/upload" is opened
    When the selects the "[FILE_<File>]" file for upload
    And the user clicks on the upload button
    Then the file "<File>" is uploaded

    Examples:
      | File     |
      | test.png |