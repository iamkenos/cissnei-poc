@DEMO
Feature: Status Codes API

  @TEST
  Scenario Outline: User sends a get request to the <End Point> status_code endpoint
    Given the user gets the "https://the-internet.herokuapp.com/status_codes/<End Point>" url
    Then the response status code is "<End Point>"

    Examples:
      | End Point |
      | 200       |
      | 404       |
      | 500       |