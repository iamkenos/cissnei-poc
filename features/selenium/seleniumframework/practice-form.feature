@DEMO
Feature: Practice Form Web Site
  This is my feature file description

  Background:
  This is my background description
    Given the page "http://www.seleniumframework.com/Practiceform/" is opened

  @TEST
  Scenario: Open and close new windows, tabs and alerts
  This is my scenario description

    When user clicks on "New Browser Window" button
    And user closes the new browser window
    And user clicks on "New Message Window" button
    And user closes the new message window
    And user clicks on "New Browser Tab" button
    And user closes the new browser tab
    And user clicks on "Alert Box" button
    And user prints the alert message
    And user closes the alert
    And user clicks on "New Browser Tab" button

  Scenario: Close unhandled alert
    When user clicks on "Alert Box" button
    And user prints the nested element text
    And user prints the random id
    And user clicks on "New Browser Tab" button

  Scenario: Find nested element
    When user prints the nested element text

