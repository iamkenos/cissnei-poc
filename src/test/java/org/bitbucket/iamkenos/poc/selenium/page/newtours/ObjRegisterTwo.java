package org.bitbucket.iamkenos.poc.selenium.page.newtours;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.SPACE;
import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.objectFieldIndexFromString;
import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

public final class ObjRegisterTwo {
    private static By txtFirstName = By.name("firstName");
    private static By txtLastName = By.name("lastName");
    private static By txtPhone = By.name("phone");
    private static By txtEmail = By.name("userName");

    private static By txtAddress = By.name("address1");
    private static By txtAddress2 = By.name("address2");
    private static By txtCity = By.name("city");
    private static By txtState = By.name("state");
    private static By txtPostal = By.name("postalCode");
    private static By ddlCountry = By.name("country");

    private static By txtUserName = By.name("email");
    private static By txtPassword = By.name("password");
    private static By txtConfirmPassword = By.name("confirmPassword");

    private static By btnSubmit = By.name("register");
    private static By pDearName = By.xpath("//b[contains(.,'Dear')]");
    private static By pNoteUserName = By.xpath("//b[contains(.,'Note')]");

    private ContactInformation contactInformation;
    private MailingInformation mailingInformation;
    private UserInformation userInformation;

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public MailingInformation getMailingInformation() {
        return mailingInformation;
    }

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public WebElement getContactInformationField(String name) throws NoSuchFieldException {
        Integer index = objectFieldIndexFromString(ContactInformation.class, name);

        switch (index) {
            case 0:
                return driver.findElement(txtFirstName);
            case 1:
                return driver.findElement(txtLastName);
            case 2:
                return driver.findElement(txtPhone);
            case 3:
                return driver.findElement(txtEmail);
            default:
                throw new NoSuchFieldException(ContactInformation.class.getSimpleName() + SPACE + name);
        }
    }

    public WebElement getMailingInformationField(String name) throws NoSuchFieldException {
        Integer index = objectFieldIndexFromString(MailingInformation.class, name);

        switch (index) {
            case 0:
                return driver.findElement(txtAddress);
            case 1:
                return driver.findElement(txtAddress2);
            case 2:
                return driver.findElement(txtCity);
            case 3:
                return driver.findElement(txtState);
            case 4:
                return driver.findElement(txtPostal);
            case 5:
                return driver.findElement(ddlCountry);
            default:
                throw new NoSuchFieldException(MailingInformation.class.getSimpleName() + SPACE + name);
        }
    }

    public WebElement getUserInformationField(String name) throws NoSuchFieldException {
        Integer index = objectFieldIndexFromString(UserInformation.class, name);

        switch (index) {
            case 0:
                return driver.findElement(txtUserName);
            case 1:
                return driver.findElement(txtPassword);
            case 2:
                return driver.findElement(txtConfirmPassword);
            default:
                throw new NoSuchFieldException(UserInformation.class.getSimpleName() + SPACE + name);
        }
    }

    public void enterContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;

        driver.sendKeys(txtFirstName, contactInformation.getFirstName());
        driver.sendKeys(txtLastName, contactInformation.getLastName());
        driver.sendKeys(txtPhone, contactInformation.getPhone());
        driver.sendKeys(txtEmail, contactInformation.getEmail());
    }

    public void enterMailingInformation(MailingInformation mailingInformation) {
        this.mailingInformation = mailingInformation;

        driver.sendKeys(txtAddress, mailingInformation.getAddress());
        driver.sendKeys(txtAddress2, mailingInformation.getAddress2());
        driver.sendKeys(txtCity, mailingInformation.getCity());
        driver.sendKeys(txtState, mailingInformation.getStateProvince());
        driver.sendKeys(txtPostal, mailingInformation.getPostalCode());
        driver.dropdownSelectText(ddlCountry, mailingInformation.getCountry());
    }

    public void enterUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;

        driver.sendKeys(txtUserName, userInformation.getUserName());
        driver.sendKeys(txtPassword, userInformation.getPassword());
        driver.sendKeys(txtConfirmPassword, userInformation.getConfirmPassword());
    }

    public void clickSubmit() {
        driver.click(btnSubmit);
    }

    public String getConfirmationSalutation() {
        return driver.getElementText(pDearName);
    }

    public String getConfirmationNote() {
        return driver.getElementText(pNoteUserName);
    }

    public List<String> getContactInfoCountries() {
        return driver.getDropdownOptionsTexts(ddlCountry);
    }

    public static class ContactInformation {
        private String firstName;
        private String lastName;
        private String phone;
        private String email;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public static class MailingInformation {
        private String address;
        private String address2;
        private String city;
        private String stateProvince;
        private String postalCode;
        private String country;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getStateProvince() {
            return stateProvince;
        }

        public void setStateProvince(String stateProvince) {
            this.stateProvince = stateProvince;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }
    }

    public static class UserInformation {
        private String userName;
        private String password;
        private String confirmPassword;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getConfirmPassword() {
            return confirmPassword;
        }

        public void setConfirmPassword(String confirmPassword) {
            this.confirmPassword = confirmPassword;
        }
    }
}
