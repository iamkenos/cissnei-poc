package org.bitbucket.iamkenos.poc.selenium.step.theinternet;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;

import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpFileDownloader {

    @When("^the user downloads the file \"([^\"]*)\"$")
    public void the_user_downloads_the_file(String arg1) throws Exception {
        driver.click(By.linkText(arg1));
    }

    @Then("^the file \"([^\"]*)\" is downloaded$")
    public void the_file_is_downloaded(String arg1) throws Exception {
        driver.thenFileDownloaded(arg1);
    }
}
