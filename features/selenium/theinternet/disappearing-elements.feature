@DEMO
Feature: Disappearing elements

  Scenario: Check elements present
    Given the page "https://the-internet.herokuapp.com/disappearing_elements" is opened
    Then the navigation tab contains at least:
      | Home       |
      | About      |
      | Contact Us |
      | Portfolio  |