package org.bitbucket.iamkenos.poc.selenium.page.newtours;

import com.google.protobuf.Descriptors.FieldDescriptor;
import org.bitbucket.iamkenos.cissnei.proto.NoSuchProtoFieldException;
import org.bitbucket.iamkenos.poc.selenium.model.newtours.ProRegister.ContactInformation;
import org.bitbucket.iamkenos.poc.selenium.model.newtours.ProRegister.MailingInformation;
import org.bitbucket.iamkenos.poc.selenium.model.newtours.ProRegister.UserInformation;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.protoFieldFromString;
import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;
import static org.bitbucket.iamkenos.poc.selenium.model.newtours.ProRegister.ContactInformation.*;
import static org.bitbucket.iamkenos.poc.selenium.model.newtours.ProRegister.MailingInformation.*;
import static org.bitbucket.iamkenos.poc.selenium.model.newtours.ProRegister.UserInformation.*;


public final class ObjRegister {
    private ContactInformation contactInformation;
    private MailingInformation mailingInformation;
    private UserInformation userInformation;

    private By txtFirstName = By.name("firstName");
    private By txtLastName = By.name("lastName");
    private By txtPhone = By.name("phone");
    private By txtEmail = By.name("userName");

    private By txtAddress = By.name("address1");
    private By txtCity = By.name("city");
    private By txtState = By.name("state");
    private By txtPostal = By.name("postalCode");
    private By ddlCountry = By.name("country");

    private By txtUserName = By.name("email");
    private By txtPassword = By.name("password");
    private By txtConfirmPassword = By.name("confirmPassword");

    private By btnSubmit = By.name("register");

    private By pDearName = By.xpath("//b[contains(.,'Dear')]");
    private By pNoteUserName = By.xpath("//b[contains(.,'Note')]");

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public MailingInformation getMailingInformation() {
        return mailingInformation;
    }

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public WebElement getContactInformationField(String name) throws NoSuchProtoFieldException {
        ContactInformation message = ContactInformation.getDefaultInstance();
        FieldDescriptor field = protoFieldFromString(message, name);

        switch (field.getNumber()) {
            case FIRSTNAME_FIELD_NUMBER:
                return driver.findElement(txtFirstName);
            case LASTNAME_FIELD_NUMBER:
                return driver.findElement(txtLastName);
            case PHONE_FIELD_NUMBER:
                return driver.findElement(txtPhone);
            case EMAIL_FIELD_NUMBER:
                return driver.findElement(txtEmail);
            default:
                throw new NoSuchProtoFieldException(message, name);
        }
    }

    public WebElement getMailingInformationField(String name) throws NoSuchProtoFieldException {
        MailingInformation message = MailingInformation.getDefaultInstance();
        FieldDescriptor field = protoFieldFromString(message, name);

        switch (field.getNumber()) {
            case ADDRESS_FIELD_NUMBER:
                return driver.findElement(txtAddress);
            case CITY_FIELD_NUMBER:
                return driver.findElement(txtCity);
            case STATEPROVINCE_FIELD_NUMBER:
                return driver.findElement(txtState);
            case POSTALCODE_FIELD_NUMBER:
                return driver.findElement(txtPostal);
            case COUNTRY_FIELD_NUMBER:
                return driver.findElement(ddlCountry);
            default:
                throw new NoSuchProtoFieldException(message, name);
        }
    }

    public WebElement getUserInformationField(String name) throws NoSuchProtoFieldException {
        UserInformation message = UserInformation.getDefaultInstance();
        FieldDescriptor field = protoFieldFromString(message, name);

        switch (field.getNumber()) {
            case USERNAME_FIELD_NUMBER:
                return driver.findElement(txtUserName);
            case PASSWORD_FIELD_NUMBER:
                return driver.findElement(txtPassword);
            case CONFIRMPASSWORD_FIELD_NUMBER:
                return driver.findElement(txtConfirmPassword);
            default:
                throw new NoSuchProtoFieldException(message, name);
        }
    }

    public void enterContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;

        driver.sendKeys(txtFirstName, contactInformation.getFirstName());
        driver.sendKeys(txtLastName, contactInformation.getLastName());
        driver.sendKeys(txtPhone, contactInformation.getPhone());
        driver.sendKeys(txtEmail, contactInformation.getEmail());
    }

    public void enterMailingInformation(MailingInformation mailingInformation) {
        this.mailingInformation = mailingInformation;

        driver.sendKeys(txtAddress, mailingInformation.getAddress());
        driver.sendKeys(txtCity, mailingInformation.getCity());
        driver.sendKeys(txtState, mailingInformation.getStateProvince());
        driver.sendKeys(txtPostal, mailingInformation.getPostalCode());
        driver.dropdownSelectText(ddlCountry, mailingInformation.getCountry());
    }

    public void enterUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;

        driver.sendKeys(txtUserName, userInformation.getUserName());
        driver.sendKeys(txtPassword, userInformation.getPassword());
        driver.sendKeys(txtConfirmPassword, userInformation.getConfirmPassword());
    }

    public void clickSubmit() {
        driver.click(btnSubmit);
    }

    public String getConfirmationSalutation() {
        return driver.getElementText(pDearName);
    }

    public String getConfirmationNote() {
        return driver.getElementText(pNoteUserName);
    }

    public List<String> getContactInfoCountries() {
        return driver.getDropdownOptionsTexts(ddlCountry);
    }
}

