package org.bitbucket.iamkenos.poc.selenium.step.theinternet;

import cucumber.api.java.en.When;
import org.openqa.selenium.By;

import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpFloatingMenu {

    @When("^the user clicks on the \"([^\"]*)\" floating menu item$")
    public void the_user_clicks_on_the_floating_menu_item(String arg1) throws Exception {
        driver.click(By.linkText(arg1));
    }
}
