package org.bitbucket.iamkenos.poc.selenium.step.common;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;

import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpCommon {
    @When("^the page \"([^\"]*)\" is opened$")
    public void the_page_is_opened(String arg1) throws Exception {
        driver.get(arg1);
    }

    @When("^the user clicks on the \"([^\"]*)\" link$")
    public void the_user_clicks_on_the_link(String arg1) throws Exception {
        driver.click(By.linkText(arg1));
    }

    @When("^the user scrolls down to the bottom of the page$")
    public void the_user_scrolls_down_to_the_bottom_of_the_page() throws Exception {
        driver.scrollToBottom();
    }

    @Then("^the page url is \"([^\"]*)\"$")
    public void the_page_url_is(String arg1) throws Exception {
        driver.thenPageUrlIs(arg1);
    }
}
