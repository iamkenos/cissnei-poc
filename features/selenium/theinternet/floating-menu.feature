@DEMO
Feature: Floating Menu

  Scenario: Click on a floating menu item
    Given the page "https://the-internet.herokuapp.com/floating_menu" is opened
    When the user scrolls down to the bottom of the page
    And the user clicks on the "Contact" floating menu item
    Then the page url is "https://the-internet.herokuapp.com/floating_menu#contact"