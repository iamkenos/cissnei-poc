package org.bitbucket.iamkenos.poc.selenium.step.theinternet;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.bitbucket.iamkenos.cissnei.Validate;
import org.bitbucket.iamkenos.poc.selenium.page.theinternet.ObjDragDrop;

import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpDragDrop {

    private ObjDragDrop objDragDrop;

    public StpDragDrop() {
        objDragDrop = new ObjDragDrop();
    }

    @When("^the user drags the box \"([^\"]*)\" to box \"([^\"]*)\"$")
    public void the_user_drags_the_box_to_box(String arg1, String arg2) throws Exception {
        objDragDrop.dragDrop(arg1, arg2);
    }

    @Then("^the boxes \"([^\"]*)\" and \"([^\"]*)\" are swapped$")
    public void the_boxes_and_are_swapped(String arg1, String arg2) throws Exception {
        Validate.equals(driver.getElementText(objDragDrop.divColumnA), arg2);
        Validate.equals(driver.getElementText(objDragDrop.divColumnB), arg1);
    }
}
