package org.bitbucket.iamkenos.poc.selenium.page.theinternet;

import org.openqa.selenium.By;

import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.TO_REPLACE;
import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ObjDynamicControls {
    private String xpathCbxACheckbox = String.format("//div[contains(.,'%s')]//input[@id='checkbox']", TO_REPLACE);
    private String xpathBtnAddRmv = String.format("//button[@id='btn'][text()='%s']", TO_REPLACE);
    private String xpathDivLoadingBar = String.format("//div[@id='loading'][contains(text(),'%s')]", TO_REPLACE);
    private String xpathLblMessage = String.format("//p[@id='message'][contains(text(),\"%s\")]", TO_REPLACE);

    public void clickAddRmvButton(String label) {
        driver.click(By.xpath(xpathBtnAddRmv.replace(TO_REPLACE, label)));
    }

    public void toggleDynamicCheckbox(String label) {
        driver.click(getDynamicCheckbox(label));
    }

    public By getDynamicCheckbox(String label) {
        return By.xpath(xpathCbxACheckbox.replace(TO_REPLACE, label));
    }

    public By getLoadingBar(String label) {
        return By.xpath(xpathDivLoadingBar.replace(TO_REPLACE, label));
    }

    public By getDynamicMessage(String label) {
        return By.xpath(xpathLblMessage.replace(TO_REPLACE, label));
    }
}