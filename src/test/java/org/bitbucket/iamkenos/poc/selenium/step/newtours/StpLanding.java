package org.bitbucket.iamkenos.poc.selenium.step.newtours;

import cucumber.api.java.en.When;
import org.bitbucket.iamkenos.poc.selenium.page.newtours.ObjLanding;

import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.transformData;
import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

public final class StpLanding {
    private ObjLanding objLanding;

    public StpLanding() {
        objLanding = new ObjLanding();
    }

    @When("^the user navigates to the new tours website$")
    public void the_user_navigates_to_the_new_tours_website() throws Exception {
        driver.get(transformData("{WEB_DEMO URL}"));
    }

    @When("^the user clicks on the \"([^\"]*)\" navigation tab$")
    public void the_user_clicks_on_the_navigation_tab(String arg1) throws Exception {
        objLanding.clickNavBar(arg1);
    }
}
