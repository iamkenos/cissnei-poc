@DEMO
Feature: Hovers

  Scenario Outline: Hover on the images and check for the dynamic section
    Given the page "https://the-internet.herokuapp.com/hovers" is opened
    When the user hovers on the "user<ID>" image
    Then the the "user<ID>" hover details is displayed
    When the user clicks on the "View profile" link
    Then the page url is "https://the-internet.herokuapp.com/users/<ID>"

    Examples:
      | ID |
      | 1  |
      | 2  |
      | 3  |