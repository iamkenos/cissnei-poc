package org.bitbucket.iamkenos.poc.selenium.page.newtours;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;


public final class ObjLanding {

    private By tdNavBars = By.xpath("//table[@border='1']//td");

    public List<WebElement> getNavBars() {
        return driver.findElements(tdNavBars);
    }

    public void clickNavBar(String text) {
        driver.click(driver.findElementFromListWithText(getNavBars(), text));
    }

}
