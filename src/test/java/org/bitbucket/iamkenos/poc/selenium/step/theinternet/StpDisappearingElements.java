package org.bitbucket.iamkenos.poc.selenium.step.theinternet;

import cucumber.api.java.en.Then;
import org.bitbucket.iamkenos.cissnei.Validate;
import org.openqa.selenium.By;

import java.util.List;

import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpDisappearingElements {

    @Then("^the navigation tab contains at least:$")
    public void the_navigation_tab_contains_at_least(List<String> arg1) throws Exception {
        List<String> actual = driver.getElementsText(By.xpath("(//ul)[1]/li"));
        Validate.contains(actual, arg1);
    }
}
