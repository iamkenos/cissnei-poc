package org.bitbucket.iamkenos.poc.selenium;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.bitbucket.iamkenos.cissnei.selenium.Driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class SeleniumHook {
    public static Scenario scenario;
    public static Driver driver;

    @Before
    public void beforeScenario(Scenario myScenario) {
        scenario = myScenario;
        driver = new Driver(scenario);
    }

    @After
    public void afterScenario() {
        driver.embedScreenshot();
        driver.quit();
    }
}
