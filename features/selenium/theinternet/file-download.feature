@DEMO
Feature: File Downloader

  Scenario Outline: Download a file
    Given the page "https://the-internet.herokuapp.com/download" is opened
    When the user downloads the file "<File Name>"
    Then the file "<File Name>" is downloaded

    Examples:
      | File Name               |
      | CaptureAccountLogin.PNG |
      | payment_methods54.png   |