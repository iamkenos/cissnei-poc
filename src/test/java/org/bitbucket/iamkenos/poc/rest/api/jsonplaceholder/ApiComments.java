package org.bitbucket.iamkenos.poc.rest.api.jsonplaceholder;

import org.bitbucket.iamkenos.cissnei.rest.Client;

import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.transformData;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class ApiComments {
    private Client client;

    private String epComments = "comments";

    public ApiComments(Client client) {
        this.client = client;
        this.client.getInstanceConfig()
                .setBaseUri(transformData("{API_DEMO URL}"));
    }

    public void retrieveComments() {
        client.get(epComments);
    }
}
