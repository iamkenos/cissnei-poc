@DEMO
Feature: Register at New Tours

  Scenario: Register as a new user [protobuf]
    Given the user navigates to the new tours website
    When the user clicks on the "REGISTER" navigation tab
    And the user enters contact information:
      | First Name | [ANUM_10]  |
      | Last Name  | [ANUM_10]  |
      | Phone      | [NUM_8]    |
      | Email      | [EMAIL_10] |
    And the user enters mailing information:
      | Address        | [ANUM_10] |
      | City           | [ANUM_10] |
      | State/Province | [ANUM_10] |
      | Postal Code    | [NUM_10]  |
      | Country        | ARGENTINA |
    And the user enters user information:
      | User Name        | [ANUM_10]  |
      | Password         | mypassword |
      | Confirm Password | mypassword |
    And the user submits the registration form
    Then the user is registered

  Scenario: Field level checks [protobuf]
    Given the user navigates to the new tours website
    When the user clicks on the "REGISTER" navigation tab
    Then the contact information "First Name" field accepts "5" alphabetic input
    And the mailing information "Address" field accepts "25" alpha numeric input
    And the user information "Password" field accepts "10" alpha numeric and special input
    And the country dropdown contains the following options:
      | ANDORRA |
      | ANGOLA  |
      | NORWAY  |

  Scenario: Register as a new user [bean]
    Given the user navigates to the new tours website
    When the user clicks on the "REGISTER" navigation tab
    And the user enters contact information2:
      | First Name | [ANUM_10]  |
      | Last Name  | [ANUM_10]  |
      | Phone      | [NUM_8]    |
      | Email      | [EMAIL_10] |
    And the user enters mailing information2:
      | Address        | [ANUM_10] |
      | Address2       | [ANUM_10] |
      | City           | [ANUM_10] |
      | State/Province | [ANUM_10] |
      | Postal Code    | [NUM_10]  |
      | Country        | ARGENTINA |
    And the user enters user information2:
      | User Name        | [ANUM_10]  |
      | Password         | mypassword |
      | Confirm Password | mypassword |
    And the user submits the registration form2
    Then the user is registered2

  Scenario: Field level checks [bean]
    Given the user navigates to the new tours website
    When the user clicks on the "REGISTER" navigation tab
    Then the contact information "First Name" field accepts "5" alphabetic input2
    And the mailing information "Address" field accepts "25" alpha numeric input2
    And the user information "Password" field accepts "10" alpha numeric and special input2
    And the country dropdown contains the following options2:
      | ANDORRA |
      | ANGOLA  |
      | NORWAY  |