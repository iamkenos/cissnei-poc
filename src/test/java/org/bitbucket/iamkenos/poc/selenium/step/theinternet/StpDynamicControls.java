package org.bitbucket.iamkenos.poc.selenium.step.theinternet;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.bitbucket.iamkenos.poc.selenium.page.theinternet.ObjDynamicControls;

import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpDynamicControls {
    private ObjDynamicControls objDynamicControls;

    public StpDynamicControls() {
        objDynamicControls = new ObjDynamicControls();
    }

    @When("^the user toggles the \"([^\"]*)\" checkbox in dynamic controls$")
    public void the_user_toggles_the_checkbox_in_dynamic_controls(String arg1) throws Exception {
        objDynamicControls.toggleDynamicCheckbox(arg1);
    }

    @When("^the user clicks on the \"([^\"]*)\" button in dynamic controls$")
    public void the_user_clicks_on_the_button_in_dynamic_controls(String arg1) throws Exception {
        objDynamicControls.clickAddRmvButton(arg1);
    }

    @Then("^the \"([^\"]*)\" checkbox is selected in dynamic controls$")
    public void the_checkbox_is_selected_in_dynamic_controls(String arg1) throws Exception {
        driver.thenElemSelected(objDynamicControls.getDynamicCheckbox(arg1));
    }

    @Then("^the \"([^\"]*)\" loading bar appears in dynamic controls$")
    public void the_loading_bar_appears_in_dynamic_controls(String arg1) throws Exception {
        driver.thenElemVisible(objDynamicControls.getLoadingBar(arg1));
    }

    @Then("^the \"([^\"]*)\" label appears in dynamic controls$")
    public void the_label_appears_in_dynamic_controls(String arg1) throws Exception {
        driver.thenElemVisible(objDynamicControls.getDynamicMessage(arg1));
    }

    @Then("^the \"([^\"]*)\" checkbox disappears in dynamic controls$")
    public void the_checkbox_disappears_in_dynamic_controls(String arg1) throws Exception {
        driver.thenElemInvisible(objDynamicControls.getDynamicCheckbox(arg1));
    }

    @Then("^the \"([^\"]*)\" loading bar disappears in dynamic controls$")
    public void the_loading_bar_disappears_in_dynamic_controls(String arg1) throws Exception {
        driver.thenElemInvisible(objDynamicControls.getLoadingBar(arg1));
    }

    @Then("^the \"([^\"]*)\" checkbox appears in dynamic controls$")
    public void the_checkbox_appears_in_dynamic_controls(String arg1) throws Exception {
        driver.thenElemVisible(objDynamicControls.getDynamicCheckbox(arg1));
    }
}