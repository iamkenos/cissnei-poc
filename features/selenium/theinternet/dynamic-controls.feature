@DEMO
Feature: Dynamic Controls

  Scenario: Work on dynamic controls
    Given the page "https://the-internet.herokuapp.com/dynamic_controls" is opened
    When the user toggles the "A checkbox" checkbox in dynamic controls
    Then the "A checkbox" checkbox is selected in dynamic controls
    When the user clicks on the "Remove" button in dynamic controls
    Then the "Wait for it..." loading bar appears in dynamic controls
    And the "It's gone!" label appears in dynamic controls
    And the "A checkbox" checkbox disappears in dynamic controls
    And the "Wait for it..." loading bar disappears in dynamic controls
    When the user clicks on the "Add" button in dynamic controls
    Then the "Wait for it..." loading bar appears in dynamic controls
    And the "It's back!" label appears in dynamic controls
    And the "A checkbox" checkbox appears in dynamic controls
    And the "Wait for it..." loading bar disappears in dynamic controls