package org.bitbucket.iamkenos.poc.selenium.step.newtours;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.bitbucket.iamkenos.cissnei.Validate;
import org.bitbucket.iamkenos.poc.selenium.model.newtours.ProRegister;
import org.bitbucket.iamkenos.poc.selenium.page.newtours.ObjRegister;
import org.bitbucket.iamkenos.poc.selenium.page.newtours.ObjRegisterTwo;

import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.SPACE;
import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.transposeAndCastOne;
import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

public final class StpRegister {
    private ObjRegister objRegister;
    private ObjRegisterTwo objRegisterTwo;

    public StpRegister() {
        objRegister = new ObjRegister();
        objRegisterTwo = new ObjRegisterTwo();
    }

    @When("^the user enters contact information:$")
    public void the_user_enters_contact_information(DataTable arg1) throws Exception {
        ProRegister.ContactInformation contactInformation = transposeAndCastOne(arg1, ProRegister.ContactInformation.getDefaultInstance());
        objRegister.enterContactInformation(contactInformation);
    }

    @When("^the user enters contact information2:$")
    public void the_user_enters_contact_information2(DataTable arg1) throws Exception {
        ObjRegisterTwo.ContactInformation contactInformation = transposeAndCastOne(arg1, ObjRegisterTwo.ContactInformation.class);
        objRegisterTwo.enterContactInformation(contactInformation);
    }

    @When("^the user enters mailing information:$")
    public void the_user_enters_mailing_information(DataTable arg1) throws Exception {
        ProRegister.MailingInformation mailingInformation = transposeAndCastOne(arg1, ProRegister.MailingInformation.getDefaultInstance());
        objRegister.enterMailingInformation(mailingInformation);
    }

    @When("^the user enters mailing information2:$")
    public void the_user_enters_mailing_information2(DataTable arg1) throws Exception {
        ObjRegisterTwo.MailingInformation mailingInformation = transposeAndCastOne(arg1, ObjRegisterTwo.MailingInformation.class);
        objRegisterTwo.enterMailingInformation(mailingInformation);
    }

    @When("^the user enters user information:$")
    public void the_user_enters_user_information(DataTable arg1) throws Exception {
        ProRegister.UserInformation userInformation = transposeAndCastOne(arg1, ProRegister.UserInformation.getDefaultInstance());
        objRegister.enterUserInformation(userInformation);
    }

    @When("^the user enters user information2:$")
    public void the_user_enters_user_information2(DataTable arg1) throws Exception {
        ObjRegisterTwo.UserInformation userInformation = transposeAndCastOne(arg1, ObjRegisterTwo.UserInformation.class);
        objRegisterTwo.enterUserInformation(userInformation);
    }

    @When("^the user submits the registration form$")
    public void the_user_submits_the_registration_form() throws Exception {
        objRegister.clickSubmit();
    }

    @When("^the user submits the registration form2$")
    public void the_user_submits_the_registration_form2() throws Exception {
        objRegisterTwo.clickSubmit();
    }

    @Then("^the user is registered$")
    public void the_user_is_registered() throws Exception {
        ProRegister.ContactInformation contactInformation = objRegister.getContactInformation();
        ProRegister.UserInformation userInformation = objRegister.getUserInformation();

        String actualSalutation = objRegister.getConfirmationSalutation();
        String expectedSalutation = String.format("Dear %s %s,", contactInformation.getFirstName(), contactInformation.getLastName());
        Validate.equals(actualSalutation, expectedSalutation);

        String actualNote = objRegister.getConfirmationNote();
        String expectedNote = String.format("Note: Your user name is %s.", userInformation.getUserName());
        Validate.equals(actualNote, expectedNote);
    }

    @Then("^the user is registered2$")
    public void the_user_is_registered2() throws Exception {
        ObjRegisterTwo.ContactInformation contactInformation = objRegisterTwo.getContactInformation();
        ObjRegisterTwo.UserInformation userInformation = objRegisterTwo.getUserInformation();

        String actualSalutation = objRegisterTwo.getConfirmationSalutation();
        String expectedSalutation = String.format("Dear %s %s,", contactInformation.getFirstName(), contactInformation.getLastName());
        Validate.equals(actualSalutation, expectedSalutation);

        String actualNote = objRegisterTwo.getConfirmationNote();
        String expectedNote = String.format("Note: Your user name is %s.", userInformation.getUserName());
        Validate.equals(actualNote, expectedNote);
    }

    @Then("^the contact information \"([^\"]*)\" field accepts \"([^\"]*)\" alphabetic input$")
    public void the_contact_information_field_accepts_alphabetic_input(String arg1, Integer arg2) throws Exception {
        driver.thenFieldAcceptsAlpha(objRegister.getContactInformationField(arg1), arg2);
    }

    @Then("^the contact information \"([^\"]*)\" field accepts \"([^\"]*)\" alphabetic input2$")
    public void the_contact_information_field_accepts_alphabetic_input2(String arg1, Integer arg2) throws Exception {
        driver.thenFieldAcceptsAlpha(objRegisterTwo.getContactInformationField(arg1), arg2);
    }

    @Then("^the mailing information \"([^\"]*)\" field accepts \"([^\"]*)\" alpha numeric input$")
    public void the_mailing_information_field_accepts_alpha_numeric_input(String arg1, Integer arg2) throws Exception {
        driver.thenFieldAcceptsAlphaNum(objRegister.getMailingInformationField(arg1), arg2);
    }

    @Then("^the mailing information \"([^\"]*)\" field accepts \"([^\"]*)\" alpha numeric input2$")
    public void the_mailing_information_field_accepts_alpha_numeric_input2(String arg1, Integer arg2) throws Exception {
        driver.thenFieldAcceptsAlphaNum(objRegisterTwo.getMailingInformationField(arg1), arg2);
    }

    @Then("^the user information \"([^\"]*)\" field accepts \"([^\"]*)\" alpha numeric and special input$")
    public void the_user_information_field_accepts_alpha_numeric_and_special_input(String arg1, Integer arg2) throws Exception {
        driver.thenFieldAcceptsAlphaNumSpec(objRegister.getUserInformationField(arg1), arg2);
        driver.embedText(driver.getElementValue(objRegister.getUserInformationField(arg1)));
    }

    @Then("^the user information \"([^\"]*)\" field accepts \"([^\"]*)\" alpha numeric and special input2$")
    public void the_user_information_field_accepts_alpha_numeric_and_special_input2(String arg1, Integer arg2) throws Exception {
        driver.thenFieldAcceptsAlphaNumSpec(objRegisterTwo.getUserInformationField(arg1), arg2);
        driver.embedText(driver.getElementValue(objRegisterTwo.getUserInformationField(arg1)));
    }

    @Then("^the country dropdown contains the following options:$")
    public void the_country_dropdown_contains_the_following_options(List<String> arg1) throws Exception {
        List<String> actual = objRegister.getContactInfoCountries();
        List<String> expected = arg1.stream().map(item -> item.concat(SPACE)).collect(Collectors.toList());

        Validate.contains(actual, expected);
    }

    @Then("^the country dropdown contains the following options2:$")
    public void the_country_dropdown_contains_the_following_options2(List<String> arg1) throws Exception {
        List<String> actual = objRegisterTwo.getContactInfoCountries();
        List<String> expected = arg1.stream().map(item -> item.concat(SPACE)).collect(Collectors.toList());

        Validate.contains(actual, expected);
    }
}

