package org.bitbucket.iamkenos.poc.selenium.step.theinternet;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.bitbucket.iamkenos.cissnei.Validate;
import org.bitbucket.iamkenos.poc.selenium.page.theinternet.ObjSortableDataTables;

import java.util.List;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpSortableDataTables {
    private ObjSortableDataTables objSortableDataTables;

    public StpSortableDataTables() {
        objSortableDataTables = new ObjSortableDataTables();
    }

    @When("^the sorts the \"([^\"]*)\" in ascending order$")
    public void the_sorts_the_in_ascending_order(String arg1) {
        objSortableDataTables.sortAscending(arg1);
    }

    @When("^the sorts the \"([^\"]*)\" in descending order$")
    public void the_sorts_the_in_descending_order(String arg1) {
        objSortableDataTables.sortDescending(arg1);
    }

    @Then("^the \"([^\"]*)\" column is sorted in ascending order$")
    public void the_column_is_sorted_in_ascending_order(String arg1) {
        if (objSortableDataTables.amountCols.contains(arg1)) {
            List<Double> actual = objSortableDataTables.getColumnValues(arg1);
            Validate.sortedAsc(actual);
        } else {
            List<String> actual = objSortableDataTables.getColumnValues(arg1);
            Validate.sortedAsc(actual);
        }
    }

    @Then("^the \"([^\"]*)\" column is sorted in descending order$")
    public void the_column_is_sorted_in_descending_order(String arg1) {
        if (objSortableDataTables.amountCols.contains(arg1)) {
            List<Double> actual = objSortableDataTables.getColumnValues(arg1);
            Validate.sortedDsc(actual);
        } else {
            List<String> actual = objSortableDataTables.getColumnValues(arg1);
            Validate.sortedDsc(actual);
        }
    }
}
