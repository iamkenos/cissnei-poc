package org.bitbucket.iamkenos.poc.selenium.step.theinternet;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;

import java.io.File;

import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.transformData;
import static org.bitbucket.iamkenos.poc.selenium.SeleniumHook.driver;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpFileUpload {

    @When("^the selects the \"([^\"]*)\" file for upload$")
    public void the_selects_the_file_for_upload(String arg1) {
        File file = new File(transformData(arg1));
        driver.sendKeys(By.id("file-upload"), file.getAbsolutePath());
    }

    @When("^the user clicks on the upload button$")
    public void the_user_clicks_on_the_upload_button() {
        driver.click(By.id("file-submit"));
    }

    @Then("^the file \"([^\"]*)\" is uploaded$")
    public void the_file_is_uploaded(String arg1) {
        driver.thenElemTextContains(By.id("uploaded-files"), arg1);
    }
}
