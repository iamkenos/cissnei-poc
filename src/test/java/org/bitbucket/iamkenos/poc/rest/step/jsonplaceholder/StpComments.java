package org.bitbucket.iamkenos.poc.rest.step.jsonplaceholder;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.bitbucket.iamkenos.cissnei.Validate;
import org.bitbucket.iamkenos.cissnei.rest.Client;
import org.bitbucket.iamkenos.poc.rest.api.jsonplaceholder.ApiComments;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static org.bitbucket.iamkenos.poc.rest.RestHook.scenario;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpComments {
    private Client client;
    private ApiComments apiComments;

    public StpComments() {
        client = new Client(scenario);
        apiComments = new ApiComments(client);
    }

    @When("^the user gets all the comments$")
    public void the_user_gets_all_the_comments() throws Exception {
        apiComments.retrieveComments();
    }

    @Then("^all the comments are retrieved$")
    public void all_the_comments_are_retrieved() throws Exception {
        client.thenOk();
    }

    @Then("^all the comments are retrieved and has:$")
    public void all_the_comments_are_retrieved_and_has(List<String> arg1) throws Exception {
        all_the_comments_are_retrieved();

        client.responseBodyAsJsonArray().forEach(item -> {
            List<String> actual = item.getAsJsonObject().entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList());
            List<String> expected = newArrayList(arg1);
            Collections.sort(actual);
            Collections.sort(expected);

            Validate.equals(actual, expected);
        });
    }
}
