@DEMO
Feature: Comments API

  @TEST
  Scenario: Get all the comments from the Json Placeholder site
    Given the user gets all the comments
    Then all the comments are retrieved and has:
      | id     |
      | postId |
      | name   |
      | email  |
      | body   |