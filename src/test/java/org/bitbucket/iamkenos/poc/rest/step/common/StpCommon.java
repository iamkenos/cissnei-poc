package org.bitbucket.iamkenos.poc.rest.step.common;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.bitbucket.iamkenos.cissnei.Validate;
import org.bitbucket.iamkenos.cissnei.rest.Client;

import static org.bitbucket.iamkenos.poc.rest.RestHook.scenario;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpCommon {
    private Client client;

    public StpCommon() {
        client = new Client(scenario);
    }

    @Given("^the user gets the \"([^\"]*)\" url$")
    public void the_user_gets_the_url(String arg1) {
        client.get(arg1);
    }

    @Then("^the response status code is \"([^\"]*)\"$")
    public void the_response_status_code_is(Integer arg1) {
        Validate.equals(client.responseStatus(), arg1);
    }
}
