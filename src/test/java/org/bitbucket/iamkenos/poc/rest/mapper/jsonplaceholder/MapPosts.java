package org.bitbucket.iamkenos.poc.rest.mapper.jsonplaceholder;

import com.google.gson.JsonObject;

import static org.bitbucket.iamkenos.cissnei.utils.JsonUtils.jsonTemplate;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class MapPosts {
    private JsonObject postTemplate() {
        return jsonTemplate("resources/rest/json-templates/jsonplaceholder/post.json", JsonObject.class);
    }
}
