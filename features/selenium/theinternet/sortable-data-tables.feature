@DEMO
Feature: Sortable Data Tables

  Scenario: Sort the columns in ascending order
    Given the page "https://the-internet.herokuapp.com/tables" is opened
    When the sorts the "Last Name" in ascending order
    Then the "Last Name" column is sorted in ascending order
    When the sorts the "First Name" in ascending order
    Then the "First Name" column is sorted in ascending order
    When the sorts the "Email" in ascending order
    Then the "Email" column is sorted in ascending order
    When the sorts the "Due" in ascending order
    Then the "Due" column is sorted in ascending order
    When the sorts the "Web Site" in ascending order
    Then the "Web Site" column is sorted in ascending order

  Scenario: Sort the columns in descending order
    Given the page "https://the-internet.herokuapp.com/tables" is opened
    When the sorts the "Last Name" in descending order
    Then the "Last Name" column is sorted in descending order
    When the sorts the "First Name" in descending order
    Then the "First Name" column is sorted in descending order
    When the sorts the "Email" in descending order
    Then the "Email" column is sorted in descending order
    When the sorts the "Due" in descending order
    Then the "Due" column is sorted in descending order
    When the sorts the "Web Site" in descending order
    Then the "Web Site" column is sorted in descending order