package org.bitbucket.iamkenos.poc.rest.step.jsonplaceholder;

import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.bitbucket.iamkenos.cissnei.Validate;
import org.bitbucket.iamkenos.cissnei.rest.Client;
import org.bitbucket.iamkenos.poc.rest.api.jsonplaceholder.ApiPosts;
import org.bitbucket.iamkenos.poc.rest.mapper.jsonplaceholder.MapPosts;
import org.bitbucket.iamkenos.poc.rest.model.jsonplaceholder.Post;

import static org.bitbucket.iamkenos.cissnei.utils.GherkinUtils.transposeAndCastOne;
import static org.bitbucket.iamkenos.cissnei.utils.JsonUtils.objectAsJsonObject;
import static org.bitbucket.iamkenos.cissnei.utils.JsonUtils.objectAsJsonString;
import static org.bitbucket.iamkenos.poc.rest.RestHook.scenario;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
public final class StpPosts {
    private Client client;
    private ApiPosts apiPosts;
    private MapPosts mapPosts;

    public StpPosts() {
        client = new Client(scenario);
        apiPosts = new ApiPosts(client);
        mapPosts = new MapPosts();
    }

    @When("^the user creates a new post:$")
    public void the_user_creates_a_new_post(DataTable arg1) throws Exception {
        //example of creating a json object from data table to a model class

        //create an object from the Post.java bean / model class using the provided info
        //from the feature file
        Post post = transposeAndCastOne(arg1, Post.class);

        //store the object as a reference for validation at a later time
        apiPosts.setPost(post);

        //map the object to a json
        String body = objectAsJsonString(post);
        apiPosts.createPost(body);
    }

    @When("^the user deletes the created post$")
    public void the_user_deletes_the_created_post() throws Exception {
        //get the newly created post
        Post post = apiPosts.getPost();
        String id = String.valueOf(post.getUserId());

        apiPosts.deletePost(id);
    }

    @When("^the user updates the created post:$")
    public void the_user_deletes_the_created_post(DataTable arg1) throws Exception {
        Post post = transposeAndCastOne(arg1, Post.class);

        //store the object as a reference for validation at a later time
        apiPosts.setPost(post);
        String id = String.valueOf(post.getUserId());
        String body = objectAsJsonString(post);

        apiPosts.updatePost(id, body);
    }

    @Then("^the post is created$")
    public void the_post_is_created() throws Exception {
        client.thenCreated();
    }

    @Then("^the post is created correctly$")
    public void the_post_is_created_correctly$() throws Exception {
        the_post_is_created();

        //comparing via JsonObject instances
        JsonObject actual = client.responseBodyAsJsonObject();
        JsonObject expected = objectAsJsonObject(apiPosts.getPost());
        Validate.equals(actual, expected);

        //alternatively, we can also compare via the model / bean instances
        Post actual2 = client.responseBodyAsObject(Post.class);
        Post expected2 = apiPosts.getPost();
        Validate.equals(actual2, expected2);
    }

    @Then("^the post is deleted")
    public void the_post_is_deleted() throws Exception {
        throw new PendingException();
    }

//    @Then("^the post is updated")
//    public void the_post_is_updated() throws Exception {
//        client.thenOk();
//    }
}
