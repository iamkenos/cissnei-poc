package org.bitbucket.iamkenos.poc.selenium;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.bitbucket.iamkenos.cissnei.runner.ParentRunner;
import org.junit.runner.RunWith;

/**
 * @author Blank Matunog (https://bitbucket.org/iamkenos/)
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        //dryRun = true,
        features = {"features/selenium/"},
        glue = {"classpath:org/bitbucket/iamkenos/poc/selenium/"},
        tags = {"@DEMO"}
)
public final class SeleniumTest extends ParentRunner {
}
